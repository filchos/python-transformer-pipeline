#!/usr/bin/env python

from distutils.core import setup

setup(
    name="tpipe",
    author="NPOLAR/EDS",
    author_email="olaf.schneider@npolar.no",
    description="A library to transform data through a pipeline",
    license="MIT",
    packages=["tpipe"],
    install_requires=["jsonschema"],
    version="0.12.4",
)
