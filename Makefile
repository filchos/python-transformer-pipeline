lint:
	black .
	flake8 --ignore E501,E721 .

test:
	python3 -m pytest tests/ --cov=tpipe

show-cov-as_html:
	coverage html
	xdg-open htmlcov/index.html
