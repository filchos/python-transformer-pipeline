import logging
import unittest

from tpipe.propertiesconverter import PropertiesConverter
from tpipe.item import Item


class CustomTestException(Exception):
    pass


def raise_exception(item):
    raise CustomTestException("BANG!")


class TestPropertyConverter(unittest.TestCase):
    def test_no_converter(self):
        vc = PropertiesConverter()
        src = Item(one="Én", two="To")
        dest = vc(src)
        self.assertEqual(src, dest)
        self.assertEqual(len(dest.log), 0)

    def test_not_changed(self):
        vc = PropertiesConverter()
        vc.register("value", lambda v: "final")
        src = Item(value="final")
        dest = vc(src)
        self.assertEqual(dest["value"], "final")
        self.assertEqual(len(dest.log), 0)

    def test_change_to_float(self):
        vc = PropertiesConverter()
        vc.register("number", lambda v: float(v))
        src = Item(number="3.1415926535")

        dest = vc(src)

        self.assertAlmostEqual(dest["number"], 3.1415926535)
        self.assertEqual(len(dest.log), 1)
        entry = dest.log[0]
        self.assertEqual(entry.level, logging.INFO)
        self.assertEqual(entry.message, "Transformed property")
        self.assertEqual(entry["key"], "number")
        self.assertEqual(entry["originalValue"], "3.1415926535")

    def test_change_to_float_and_fail(self):
        vc = PropertiesConverter()
        vc.register("number", lambda v: float(v))
        src = Item(number="pi")

        dest = vc(src)

        self.assertEqual(dest["number"], "pi")
        self.assertEqual(len(dest.log), 1)
        entry = dest.log[0]
        self.assertEqual(entry["key"], "number")
        self.assertEqual(entry["error"], "ValueError")
        self.assertIn("could not convert string to float", entry["message"])

    def test_add_property(self):
        vc = PropertiesConverter()
        vc.register("added", lambda v: 42)
        src = Item()

        dest = vc(src)

        self.assertEqual(dest.get("added"), 42)
        self.assertEqual(len(dest.log), 1)
        entry = dest.log[0]
        self.assertEqual(entry.level, logging.INFO)
        self.assertEqual(entry.message, "Added property")
        self.assertEqual(entry["key"], "added")

    def test_do_not_add_property_with_null_value(self):
        vc = PropertiesConverter()
        vc.register("ignored", lambda v: None)
        src = Item()

        dest = vc(src)

        self.assertNotIn("ignored", dest)
        self.assertEqual(len(dest.log), 0)

    def test_add_property_and_fail(self):
        vc = PropertiesConverter()
        vc.register("added", raise_exception)
        src = Item()

        dest = vc(src)

        self.assertNotIn("added", dest)

        self.assertEqual(len(dest.log), 1)
        entry = dest.log[0]

        self.assertEqual(entry["key"], "added")
        self.assertEqual(entry["error"], "CustomTestException")
        self.assertEqual(entry["message"], "BANG!")

    def test_multiple_passing(self):
        vc = (
            PropertiesConverter()
            .register("number", lambda v: float(v))
            .register("greeting", lambda v: "Hei!")
        )
        src = Item(number="3.1415926535")

        dest = vc(src)

        self.assertAlmostEqual(dest["number"], 3.1415926535)
        self.assertAlmostEqual(dest["greeting"], "Hei!")
        self.assertEqual(len(dest.log), 2)

    def test_last_registered_converter(self):
        vc = (
            PropertiesConverter()
            .register("nochange", lambda v: v)
            .register("greeting", raise_exception)
            .register("nospace", lambda v: v.replace(" ", ""))
        )
        src = Item(nospace=" Hello World!   ", nochange="static")

        dest = vc(src)
        self.assertEqual(dest["nospace"], "HelloWorld!")
        self.assertEqual(len(dest.log), 2)
        self.assertEqual(dest.log[0]["key"], "greeting")
        self.assertEqual(dest.log[0]["error"], "CustomTestException")
        self.assertEqual(dest.log[1]["key"], "nospace")

    def test_strict_mode(self):
        vc = (
            PropertiesConverter(strict=True)
            .register("a", lambda v: v)
            .register("b", lambda v: v)
        )

        src = Item(a=1, b=2)
        dest = vc(src)

        self.assertEqual(len(dest.log), 0)

        src = Item(a=1, b=2, c=3)
        dest = vc(src)

        self.assertEqual(len(dest.log), 1)
        self.assertEqual(dest.log[0].details["keys"], ["c"])
