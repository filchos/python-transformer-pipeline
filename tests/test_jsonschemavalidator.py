import json
import logging
import os
import tempfile
import unittest

from tpipe.item import Item
from tpipe.jsonschemavalidator import JSONSchemaValidator


SCHEMA = """
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "properties": {
    "age": {
      "type": "number",
      "minimum": 0
    }
  }
}
"""

SCHEMA_WITH_REF = """
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "properties": {
    "age": {
      "$ref": "_age.schema.json"
    }
  }
}
"""

SCHEMA_REF_AGE = """
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "number",
  "minimum": 0
}
"""


class TestJSONSchemaValidator(unittest.TestCase):
    def test_success(self):
        data = {"age": 95}
        item = Item(data)
        validator = JSONSchemaValidator(json.loads(SCHEMA))

        validator(item)
        self.assertEqual(len(item.log), 0)

    def test_failure(self):
        data = {"age": -1}
        item = Item(data)
        validator = JSONSchemaValidator(json.loads(SCHEMA))

        validator(item)
        self.assertEqual(len(item.log), 1)
        entry = item.log[0]
        self.assertEqual(entry.level, logging.ERROR)
        self.assertIn("-1 is less than the minimum of 0", entry["message"])
        self.assertEqual(entry["path"], ["age"])
        self.assertEqual(entry["validator"], "minimum")
        self.assertEqual(entry["value"], -1)

    def test_validate_with_refs_failure(self):
        with tempfile.TemporaryDirectory() as tempdir:
            main_path = os.path.join(tempdir, "main.schema.json")

            # save schema files into temp dir
            with open(main_path, "w") as fp:
                fp.write(SCHEMA_WITH_REF)
            with open(os.path.join(tempdir, "_age.schema.json"), "w") as fp:
                fp.write(SCHEMA_REF_AGE)

            data = {"age": -1}
            item = Item(data)
            validator = JSONSchemaValidator.load(main_path, tempdir)
            validator(item)

        self.assertEqual(len(item.log), 1)
        entry = item.log[0]
        self.assertEqual(entry.level, logging.ERROR)
        self.assertIn("-1 is less than the minimum of 0", entry["message"])
        self.assertEqual(entry["path"], ["age"])
        self.assertEqual(entry["validator"], "minimum")
        self.assertEqual(entry["value"], -1)
