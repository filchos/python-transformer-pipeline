import logging
import unittest

from tpipe.item import Item
from tpipe.duplicatefinder import DuplicateFinder


class TestDuplicateFinder(unittest.TestCase):
    def test(self):
        def fn(item):
            return item["id"]

        df = DuplicateFinder(fn)

        it1 = Item({"id": 1, "row": 1})
        df(it1)
        self.assertEqual(len(it1.log), 0)

        it2 = Item({"id": 2, "row": 2})
        df(it2)
        self.assertEqual(len(it2.log), 0)

        it3 = Item({"id": 1, "row": 3})
        df(it3)
        self.assertEqual(len(it3.log), 1)
        entry = it3.log[0]
        self.assertEqual(entry.message, "Found duplicate(s)")
        self.assertEqual(entry.level, logging.ERROR)
        ids = [item["row"] for item in entry["duplicates"]]
        self.assertEqual(ids, [1])
