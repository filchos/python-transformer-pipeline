import itertools
import unittest

from tpipe.item import Item
from tpipe.process import Process, apply, apply_on_item


# example generator


def example_generator():
    for prime in [2, 3, 5, 7, 11]:
        yield Item({"n": prime})


# example transformers


def square(item):
    item["n"] = item["n"] ** 2
    return item


def increase(item):
    item["n"] = item["n"] + 1
    return item


# example filters


def isodd(item):
    return item["n"] % 2 == 1


def isonedigit(item):
    return item["n"] < 10


class TestPipeline(unittest.TestCase):
    def test_blankio(self):
        process = Process()
        iterator = apply(example_generator(), process)

        self.assertTrue(hasattr(iterator, "__next__"))
        self.assertTrue(hasattr(iterator, "__iter__"))
        self.assertTrue(callable(iterator.__iter__))
        self.assertTrue(iterator.__iter__() is iterator)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [2, 3, 5, 7, 11])

    def test_reuse_process(self):
        process = Process()

        iterator = apply(example_generator(), process)
        list(iterator)

        iterator = apply(example_generator(), process)
        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [2, 3, 5, 7, 11])

    def test_map(self):
        process = Process().map(square).map(increase)
        iterator = apply(example_generator(), process)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [5, 10, 26, 50, 122])

    def test_filter(self):
        process = Process().filter(isodd).filter(isonedigit)
        iterator = apply(example_generator(), process)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [3, 5, 7])

    def test_filter_and_map(self):
        process = Process().filter(isodd).map(increase).filter(isonedigit).map(square)
        iterator = apply(example_generator(), process)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [16, 36, 64])

    def test_filter_and_map_in_two_processes(self):
        process1 = Process().filter(isodd).map(increase)
        process2 = Process().filter(isonedigit).map(square)
        iterator = apply(example_generator(), process1, process2)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [16, 36, 64])

    def test_filter_and_map_in_two_apply_calls(self):
        process1 = Process().filter(isodd).map(increase)
        process2 = Process().filter(isonedigit).map(square)
        iterator = apply(apply(example_generator(), process1), process2)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [16, 36, 64])

    def test_generator_only_once(self):
        process = Process().filter(isodd).map(increase).filter(isonedigit).map(square)
        iterator = apply(example_generator(), process)

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [16, 36, 64])

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [])

    def test_use_tee(self):
        process = Process().filter(isodd).map(increase).filter(isonedigit).map(square)
        iterator = apply(example_generator(), process)

        iter1, iter2 = itertools.tee(iterator, 2)

        numbers = [item["n"] for item in iter1]
        self.assertEqual(numbers, [16, 36, 64])

        numbers = [item["n"] for item in iter2]
        self.assertEqual(numbers, [16, 36, 64])

    def test_run(self):
        process = Process().filter(isodd).map(increase).filter(isonedigit).map(square)
        iterator = process.run(example_generator())

        numbers = [item["n"] for item in iterator]
        self.assertEqual(numbers, [16, 36, 64])

    def test_apply_on_item(self):
        process = Process().filter(isodd).map(increase).filter(isonedigit).map(square)

        passing = [1, 3]
        for n in passing:
            want = (n + 1) ** 2
            item = apply_on_item(Item(n=n), process)
            self.assertEqual(item["n"], want)

        failing = [
            2,  # fail on isOdd,
            9,  # fail on isonedigit
        ]
        for n in failing:
            item = apply_on_item(Item(n=n), process)
            self.assertIsNone(item)
