import json
import logging
import os
import unittest
import uuid

from tpipe.io import CSVReader, CSVWriter, NDJSONReader
from tpipe.item import Item


def create_random_path():
    return "/tmp/{}.csv".format(uuid.uuid4())


def write_csv(path, lines):
    with open(path, "w") as f:
        f.write("i,sqr,comment\n")
        for i in range(lines):
            msg = ""
            if i == 0:
                msg = "First line"
            f.write("%d,%d,%s\n" % (i, i * i, msg))


def write_ndjson(path):
    with open(path, "w") as f:
        for i in range(3):
            item = {"i": i, "sqr": i * i}
            f.write(json.dumps(item) + "\n")


def write_tsv(path, lines):
    with open(path, "w") as f:
        f.write("i\tsqr\n")
        for i in range(lines):
            f.write("%d\t%d\n" % (i, i * i))


class TestCSVReader(unittest.TestCase):
    def test_basic(self):
        path = "/tmp/test.csv"
        write_csv(path, 3)

        generator = CSVReader(path)
        self.assertEqual(generator.headers, ["i", "sqr", "comment"])
        items = list(generator)
        self.assertEqual(len(items), 3)
        self.assertEqual(items[0]["comment"], "First line")
        self.assertEqual(items[2]["sqr"], "4")  # each value is a string
        self.assertEqual(items[2].meta["index"], 2)
        for i in range(3):
            self.assertEqual(items[i].meta["filename"], "test.csv")

        # cleanup
        os.remove(path)

    def test_tsv(self):
        path = "/tmp/test.tsv"
        write_tsv(path, 3)

        generator = CSVReader(path, delimiter="\t", quotechar='"')
        items = list(generator)
        self.assertEqual(len(items), 3)
        self.assertEqual(items[2]["sqr"], "4")  # each value is a string

    def test_iterator(self):
        path = "/tmp/test.tsv"
        write_csv(path, 3)

        generator = CSVReader(path)
        try:
            item = next(generator)
            self.assertEqual(item, {"i": "0", "sqr": "0", "comment": "First line"})
        except Exception as e:
            self.fail(f"next function on TSVReader raised exception {e}")

        # cleanup
        os.remove(path)


class TestCSVWriter(unittest.TestCase):
    def test_import_export_equality(self):
        path = create_random_path()
        write_csv(path, 8)

        with open(path, mode="r") as f:
            original = f.read()

        pipe = CSVReader(path)

        path = create_random_path()
        CSVWriter().write(pipe, path)

        with open(path, mode="r") as f:
            saved = f.read()

        self.assertEqual(original, saved)

    def test_preserve_key_order(self):
        data = [Item(sun=0.0, mercury=0.39, venus=0.72), Item(sun=0.0, earth=1.0)]

        path = create_random_path()
        CSVWriter().write(iter(data), path)

        data2 = list(CSVReader(path))
        self.assertEqual(len(data2), 2)
        self.assertEqual(list(data2[0].keys()), ["sun", "mercury", "venus", "earth"])


class TestNDJSONReader(unittest.TestCase):
    def test_basic(self):
        path = "/tmp/test.ndjson"
        write_ndjson(path)

        # test
        pipe = NDJSONReader(path)
        items = list(pipe)
        self.assertEqual(len(items), 3)
        self.assertEqual(items[2]["sqr"], 4)
        self.assertEqual(items[2].meta["index"], 2)
        self.assertEqual(items[2].meta["filename"], "test.ndjson")

        # cleanup
        os.remove(path)

    def test_invalid_line(self):
        path = "/tmp/test-invalid.ndjson"

        # create invalid NDJSON file
        with open(path, "w") as f:
            f.write('{"i":6,"sqr":36}\n"')
            f.write("this is no JSON!")

        # test
        pipe = NDJSONReader(path)
        items = list(pipe)
        self.assertEqual(len(items), 2)
        item = items[1]
        self.assertEqual(len(item.keys()), 0)
        self.assertEqual(len(item.log), 1)
        entry = item.log[0]
        self.assertEqual(entry.level, logging.CRITICAL)
        self.assertEqual(entry["error"], "JSONDecodeError")

        # cleanup
        os.remove(path)

    def test_iterator(self):
        path = "/tmp/test.ndjson"
        write_ndjson(path)

        # create NDJSON file
        with open(path, "w") as f:
            for i in range(3):
                item = {"i": i, "sqr": i * i}
                f.write(json.dumps(item) + "\n")

        # test
        pipe = NDJSONReader(path)
        try:
            next(pipe)
        except Exception as e:
            self.fail(f"next function on NDJSONReader raised exception {e}")

        # cleanup
        os.remove(path)
